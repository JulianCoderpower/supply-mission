# The Supply Mission

Rick and Daryl are in supply mission. 
They're entering in a shop. 
There are stuffs, walkers and remains inside.
They have to eliminate walkers, reach the remains and retrieve food, drinks and meds.

The function shop have a string argument.

The string contains "drink", "food", "meds", "ammo", etc...

You have to replace all "walker" by "dead".
You have to replace all "remains" by "stuff".
Replace all elements except "drink", "food", "meds" and "dead" by "stuff".

`
    string = "food remains drink meds walker ammo"
`

after


`
    string = "food stuff drink meds dead stuff
`

Good luck survivor!


SPECS

Must return a string
Don\'t forget to take the bag!

Must return a string where "walker" are replace by "dead"
All the walkers are dead!

Must return a string where "remains" are replace by "stuff"
I\'ve found stuff on remains!

Must return a string containing "food", "drink", "meds", "dead" and "stuff"
The walkers are dead and the bag is full! Go back to Alexandria!
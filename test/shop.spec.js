var expect = require('expect.js');
var shop = require('../sources/shop.js');

suite('Must return a string', function(){

    test('Don\'t forget to take the bag!', function(){
        var elements = 'food beverage walker remains food beverage beverage walker ammo food remains gun meds walker remains meds food';
        var result = shop(elements);
        expect(result).to.be.a('string');
    });
});

suite('Must return a string where "walker" are replace by "dead"', function(){

    test('All the walkers are dead!', function(){
        var elements = 'food beverage walker supplies food beverage beverage walker food supplies meds walker supplies meds food';
        var result = shop(elements);
        expect(result).not.to.contain('walker');
        expect(result).to.contain('dead');
        expect(result).to.be('food beverage dead supplies food beverage beverage dead food supplies meds dead supplies meds food');

        var elements2 = 'walker meds food walker meds food beverage beverage walker food beverage food walker meds food meds beverage food walker walker beverage';
        var result2 = shop(elements2);
        expect(result2).not.to.contain('walker');
        expect(result).to.contain('dead');
        expect(result2).to.be('dead meds food dead meds food beverage beverage dead food beverage food dead meds food meds beverage food dead dead beverage');
    });

});

suite('Must return a string where "remains" are replace by "supplies"', function(){

    test('I\'ve found supplies on remains!', function(){
        var elements = 'remains';
        var result = shop(elements);
        expect(result).not.to.contain('remains');
        expect(result).to.contain('supplies');
        expect(result).to.be.eql('supplies');

        var elements2 = 'remains meds food remains meds food beverage remains beverage food beverage food remains remains meds food meds beverage food remains beverage';
        var result2 = shop(elements2);
        expect(result2).not.to.contain('remains');
        expect(result).to.contain('supplies');
        expect(result2).to.be.eql('supplies meds food supplies meds food beverage supplies beverage food beverage food supplies supplies meds food meds beverage food supplies beverage');
    });

});

suite('Must return a string containing "food", "beverage", "meds", "dead" and "supplies"', function(){

    test('The walkers are dead and the bag is full! Go back to Alexandria!', function(){
        var elements = 'food beverage walker supplies food beverage beverage walker ammo food supplies gun meds walker supplies meds food';
        var result = shop(elements);
        expect(result).not.to.contain('walker');
        expect(result).to.contain('dead');
        expect(result).to.contain('food');
        expect(result).to.contain('beverage');
        expect(result).to.contain('meds');
        expect(result).to.contain('supplies');
        expect(result).to.be('food beverage dead supplies food beverage beverage dead supplies food supplies supplies meds dead supplies meds food');

        var elements2 = 'remains walker meds food walker cigarettes remains meds food beverage remains beverage walker food beverage ammo food remains gun walker cigarettes remains meds food meds beverage ammo food remains gun walker cigarettes walker beverage';
        var result2 = shop(elements2);
        expect(result2).not.to.contain('walker');
        expect(result).to.contain('dead');
        expect(result).to.contain('food');
        expect(result).to.contain('beverage');
        expect(result).to.contain('meds');
        expect(result).to.contain('supplies');
        expect(result2).to.be('supplies dead meds food dead supplies supplies meds food beverage supplies beverage dead food beverage supplies food supplies supplies dead supplies supplies meds food meds beverage supplies food supplies supplies dead supplies dead beverage');
    });

});





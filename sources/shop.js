module.exports = function shop(elements){
    return elements
        .replace(/walker/g, 'dead')
        .replace(/remains/g, 'supplies')
        .replace(/\b(?!(beverage|food|meds|dead)\b)\w+\b/g, 'supplies');
};
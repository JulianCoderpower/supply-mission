# The Supply Mission

Rick and Daryl are on a mission to find something to eat and drink, as well as meds and other supplies. 
They find a shop with everything they need but there are walkers and dead bodies all around. 
They have to kill the walkers, search the remains and retrieve the food, beverages, meds and supplies.

The function **shop** have a string argument.

The string contains "beverage", "food", "meds", "ammo", etc...

You have to replace all "walker" by "dead".
You have to replace all "remains" by "supplies".
Replace all elements by "supplies" except for "beverage", "food", "meds" and "dead".

`
    var string = "food remains beverage meds walker ammo"
`

after


`
    var string = "food supplies drink meds dead supplies
`




Good luck,  survivor!
